FROM python:3.10.4-slim-bullseye

RUN apt-get update && apt-get install -y gnupg software-properties-common curl jq unzip cowsay

RUN apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"

RUN curl -fsSL https://apt.releases.hashicorp.com/gpg | apt-key add -

RUN apt-get update && apt-get install terraform

RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" \
&& unzip awscliv2.zip \
&&  ./aws/install \
&& rm awscliv2.zip
