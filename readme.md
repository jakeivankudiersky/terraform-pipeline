# Terraform GitLab Pipeline

A GitLab CI pipeline that allows Terraform to plan and apply in 2 environments.
alpha & beta

Requirements
* AWS Account
* IAM Credentials for IAM Role with sufficient priveledge to create necessary resources
* GitLab account
* Maintainer access to Gitlab project CI/CD settings
* S3 backend for each environment


## Using tfvars 
utilising .tfvars files will allow us to use the same underlying code, defining a value to the variable through using the flag  -var-file= to the plan and apply commands.


```sh
 - terraform apply -var-file="environment-vars/alpha.tfvars"
```

## Remote State file (S3)

We require a s3 bucket in each of our environments. This can be completed manually or cia the aws cli using this command. In this instance we want an alpha and a beta s3 bucket.

The bucket name is defined in the `varaibles` section of the job. The Role/User of the Runner must have permissions to access the file.

## PUshing the custom Terraform Image to your Container Regitry

```docker
docker login registry.gitlab.com
docker build -t registry.gitlab.com/<your project> -f Dockerfile .
docker push -t registry.gitlab.com/<your project>
```

This path should be added to the `tf_build_image` variable.

License
----

MIT


[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)


   [dill]: <https://github.com/joemccann/dillinger>
   [git-repo-url]: <https://github.com/joemccann/dillinger.git>
   [john gruber]: <http://daringfireball.net>
   [df1]: <http://daringfireball.net/projects/markdown/>
   [markdown-it]: <https://github.com/markdown-it/markdown-it>
   [Ace Editor]: <http://ace.ajax.org>
   [node.js]: <http://nodejs.org>
   [Twitter Bootstrap]: <http://twitter.github.com/bootstrap/>
   [jQuery]: <http://jquery.com>
   [@tjholowaychuk]: <http://twitter.com/tjholowaychuk>
   [express]: <http://expressjs.com>
   [AngularJS]: <http://angularjs.org>
   [Gulp]: <http://gulpjs.com>

   [PlDb]: <https://github.com/joemccann/dillinger/tree/master/plugins/dropbox/README.md>
   [PlGh]: <https://github.com/joemccann/dillinger/tree/master/plugins/github/README.md>
   [PlGd]: <https://github.com/joemccann/dillinger/tree/master/plugins/googledrive/README.md>
   [PlOd]: <https://github.com/joemccann/dillinger/tree/master/plugins/onedrive/README.md>
   [PlMe]: <https://github.com/joemccann/dillinger/tree/master/plugins/medium/README.md>
   [PlGa]: <https://github.com/RahulHP/dillinger/blob/master/plugins/googleanalytics/README.md>
